/*
 * Borrowed from:
 *
 * Mesa 3-D graphics library
 * Version:  3.0
 * Copyright (C) 1995-1998  Brian Paul
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef GLPP_H
#define GLPP_H
#include <GL/gl.h>

/*
 *
 * Data types (may be architecture dependent in some cases)
 *
 */

/*  C type		GL type		storage                            */
/*-------------------------------------------------------------------------*/
//typedef void		GLvoid
//typedef unsigned char	GLboolean
//typedef signed char	GLbyte;		/* 1-byte signed */
//typedef short		GLshort;	/* 2-byte signed */
//typedef int		GLint;		/* 4-byte signed */
//typedef unsigned char	GLubyte;	/* 1-byte unsigned */
//typedef unsigned short	GLushort;	/* 2-byte unsigned */
//typedef unsigned int	GLuint;		/* 4-byte unsigned */
//typedef int		GLsizei;	/* 4-byte signed */
//typedef float		GLfloat;	/* single precision float */
//typedef float		GLclampf;	/* single precision float in [0,1] */
//typedef double		GLdouble;	/* double precision float */
//typedef double		GLclampd;	/* double precision float in [0,1] */



inline void glGet( GLenum pname, GLboolean *params )
  {glGetBooleanv(pname,params );}

inline void glGet( GLenum pname, GLdouble *params )
  {glGetDoublev(pname,params );}

inline void glGet( GLenum pname, GLfloat *params )
  {glGetFloatv(pname,params );}

inline void glGet( GLenum pname, GLint *params )
  {glGetIntegerv(pname,params );}


inline void glLoadMatrix( const GLdouble m[16] )
  {glLoadMatrixd( m );}
inline void glLoadMatrix( const GLfloat m[16] )
  {glLoadMatrixf( m );}

inline void glMultMatrix( const GLdouble m[16] )
  {glMultMatrixd( m );}
inline void glMultMatrix( const GLfloat m[16] )
  {glMultMatrixf( m );}

inline void glScale( GLdouble x, GLdouble y, GLdouble z )
  {glScaled(x,y,z);}
inline void glScale( GLfloat x, GLfloat y, GLfloat z )
  {glScalef(x,y,z);}

inline void glTranslate( GLdouble x, GLdouble y, GLdouble z )
  {glTranslated(x,y,z);}
inline void glTranslate( GLfloat x, GLfloat y, GLfloat z )
  {glTranslatef(x,y,z);}

inline void glRotate( GLdouble a,GLdouble x, GLdouble y, GLdouble z )
  {glRotated(a,x,y,z);}
inline void glRotate( GLfloat a,GLfloat x, GLfloat y, GLfloat z )
  {glRotatef(a,x,y,z);}



/*
 * Drawing Functions
 */

inline void glVertex( GLdouble x, GLdouble y ) {glVertex2d(x,y);}
inline void glVertex( GLfloat  x, GLfloat  y ) {glVertex2f(x,y);}
inline void glVertex( GLint    x, GLint    y ) {glVertex2i(x,y);}
inline void glVertex( GLshort  x, GLshort  y ) {glVertex2s(x,y);}

inline void glVertex( GLdouble x, GLdouble y, GLdouble z ) {glVertex3d(x,y,z);}
inline void glVertex( GLfloat  x, GLfloat  y, GLfloat  z ) {glVertex3f(x,y,z);}
inline void glVertex( GLint    x, GLint    y, GLint    z ) {glVertex3i(x,y,z);}
inline void glVertex( GLshort  x, GLshort  y, GLshort  z ) {glVertex3s(x,y,z);}

inline void glVertex( GLdouble x, GLdouble y, GLdouble z, GLdouble w ) {glVertex4d(x,y,z,w);}
inline void glVertex( GLfloat  x, GLfloat  y, GLfloat  z, GLfloat  w ) {glVertex4f(x,y,z,w);}
inline void glVertex( GLint    x, GLint    y, GLint    z, GLint    w ) {glVertex4i(x,y,z,w);}
inline void glVertex( GLshort  x, GLshort  y, GLshort  z, GLshort  w ) {glVertex4s(x,y,z,w);}

inline void glVertex2( const GLdouble v[2] ) {glVertex2dv(v);}
inline void glVertex2( const GLfloat  v[2] ) {glVertex2fv(v);}
inline void glVertex2( const GLint    v[2] ) {glVertex2iv(v);}
inline void glVertex2( const GLshort  v[2] ) {glVertex2sv(v);}

inline void glVertex3( const GLdouble v[3] ) {glVertex3dv(v);}
inline void glVertex3( const GLfloat  v[3] ) {glVertex3fv(v);}
inline void glVertex3( const GLint    v[3] ) {glVertex3iv(v);}
inline void glVertex3( const GLshort  v[3] ) {glVertex3sv(v);}

inline void glVertex4( const GLdouble v[4] ) {glVertex4dv(v);}
inline void glVertex4( const GLfloat  v[4] ) {glVertex4fv(v);}
inline void glVertex4( const GLint    v[4] ) {glVertex4iv(v);}
inline void glVertex4( const GLshort  v[4] ) {glVertex4sv(v);}


inline void glNormal( GLbyte   nx, GLbyte   ny, GLbyte   nz ) {glNormal3b(nx,ny,nz);}
inline void glNormal( GLdouble nx, GLdouble ny, GLdouble nz ) {glNormal3d(nx,ny,nz);}
inline void glNormal( GLfloat  nx, GLfloat  ny, GLfloat  nz ) {glNormal3f(nx,ny,nz);}
inline void glNormal( GLint    nx, GLint    ny, GLint    nz ) {glNormal3i(nx,ny,nz);}
inline void glNormal( GLshort  nx, GLshort  ny, GLshort  nz ) {glNormal3s(nx,ny,nz);}

inline void glNormal( const GLbyte   v[3] ) {glNormal3bv(v);}
inline void glNormal( const GLdouble v[3] ) {glNormal3dv(v);}
inline void glNormal( const GLfloat  v[3] ) {glNormal3fv(v);}
inline void glNormal( const GLint    v[3] ) {glNormal3iv(v);}
inline void glNormal( const GLshort  v[3] ) {glNormal3sv(v);}


inline void glIndex( GLdouble c ) {glIndexd(c);}
inline void glIndex( GLfloat  c ) {glIndexf(c);}
inline void glIndex( GLint    c ) {glIndexi(c);}
inline void glIndex( GLshort  c ) {glIndexs(c);}
inline void glIndex( GLubyte  c ) {glIndexub(c);}

inline void glIndex( const GLdouble *c ) {glIndexdv(c);}
inline void glIndex( const GLfloat  *c ) {glIndexfv(c);}
inline void glIndex( const GLint    *c ) {glIndexiv(c);}
inline void glIndex( const GLshort  *c ) {glIndexsv(c);}
inline void glIndex( const GLubyte  *c ) {glIndexubv(c);}

inline void glColor( GLbyte   red, GLbyte   green, GLbyte   blue ) {glColor3b(red,green,blue);}
inline void glColor( GLdouble red, GLdouble green, GLdouble blue ) {glColor3d(red,green,blue);}
inline void glColor( GLfloat  red, GLfloat  green, GLfloat  blue ) {glColor3f(red,green,blue);}
inline void glColor( GLint    red, GLint    green, GLint    blue ) {glColor3i(red,green,blue);}
inline void glColor( GLshort  red, GLshort  green, GLshort  blue ) {glColor3s(red,green,blue);}
inline void glColor( GLubyte  red, GLubyte  green, GLubyte  blue ) {glColor3ub(red,green,blue);}
inline void glColor( GLuint   red, GLuint   green, GLuint   blue ) {glColor3ui(red,green,blue);}
inline void glColor( GLushort red, GLushort green, GLushort blue ) {glColor3us(red,green,blue);}

inline void glColor( GLbyte   red, GLbyte   green, GLbyte   blue, GLbyte   alpha )
  {glColor4b(red,green,blue,alpha);}
inline void glColor( GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha )
  {glColor4d(red,green,blue,alpha);}
inline void glColor( GLfloat  red, GLfloat  green, GLfloat  blue, GLfloat  alpha )
  {glColor4f(red,green,blue,alpha);}
inline void glColor( GLint    red, GLint    green, GLint    blue, GLint    alpha )
  {glColor4i(red,green,blue,alpha);}
inline void glColor( GLshort  red, GLshort  green, GLshort  blue, GLshort  alpha )
  {glColor4s(red,green,blue,alpha);}
inline void glColor( GLubyte  red, GLubyte  green, GLubyte  blue, GLubyte  alpha )
  {glColor4ub(red,green,blue,alpha);}
inline void glColor( GLuint   red, GLuint   green, GLuint   blue, GLuint   alpha )
  {glColor4ui(red,green,blue,alpha);}
inline void glColor( GLushort red, GLushort green, GLushort blue, GLushort alpha )
  {glColor4us(red,green,blue,alpha);}


inline void glColor3( const GLbyte   v[3] ) {glColor3bv(v);}
inline void glColor3( const GLdouble v[3] ) {glColor3dv(v);}
inline void glColor3( const GLfloat  v[3] ) {glColor3fv(v);}
inline void glColor3( const GLint    v[3] ) {glColor3iv(v);}
inline void glColor3( const GLshort  v[3] ) {glColor3sv(v);}
inline void glColor3( const GLubyte  v[3] ) {glColor3ubv(v);}
inline void glColor3( const GLuint   v[3] ) {glColor3uiv(v);}
inline void glColor3( const GLushort v[3] ) {glColor3usv(v);}

inline void glColor4( const GLbyte   v[4] ) {glColor4bv(v);}
inline void glColor4( const GLdouble v[4] ) {glColor4dv(v);}
inline void glColor4( const GLfloat  v[4] ) {glColor4fv(v);}
inline void glColor4( const GLint    v[4] ) {glColor4iv(v);}
inline void glColor4( const GLshort  v[4] ) {glColor4sv(v);}
inline void glColor4( const GLubyte  v[4] ) {glColor4ubv(v);}
inline void glColor4( const GLuint   v[4] ) {glColor4uiv(v);}
inline void glColor4( const GLushort v[4] ) {glColor4usv(v);}


inline void glTexCoord( GLdouble s ) {glTexCoord1d(s);}
inline void glTexCoord( GLfloat  s ) {glTexCoord1f(s);}
inline void glTexCoord( GLint    s ) {glTexCoord1i(s);}
inline void glTexCoord( GLshort  s ) {glTexCoord1s(s);}

inline void glTexCoord( GLdouble s, GLdouble t ) {glTexCoord2d(s,t);}
inline void glTexCoord( GLfloat  s, GLfloat  t ) {glTexCoord2f(s,t);}
inline void glTexCoord( GLint    s, GLint    t ) {glTexCoord2i(s,t);}
inline void glTexCoord( GLshort  s, GLshort  t ) {glTexCoord2s(s,t);}

inline void glTexCoord( GLdouble s, GLdouble t, GLdouble r ) {glTexCoord3d(s,t,r);}
inline void glTexCoord( GLfloat  s, GLfloat  t, GLfloat  r ) {glTexCoord3f(s,t,r);}
inline void glTexCoord( GLint    s, GLint    t, GLint    r ) {glTexCoord3i(s,t,r);}
inline void glTexCoord( GLshort  s, GLshort  t, GLshort  r ) {glTexCoord3s(s,t,r);}

inline void glTexCoord( GLdouble s, GLdouble t, GLdouble r, GLdouble q ) {glTexCoord4d(s,t,r,q);}
inline void glTexCoord( GLfloat  s, GLfloat  t, GLfloat  r, GLfloat  q ) {glTexCoord4f(s,t,r,q);}
inline void glTexCoord( GLint    s, GLint    t, GLint    r, GLint    q ) {glTexCoord4i(s,t,r,q);}
inline void glTexCoord( GLshort  s, GLshort  t, GLshort  r, GLshort  q ) {glTexCoord4s(s,t,r,q);}

inline void glTexCoord1( const GLdouble v[1] ) {glTexCoord1dv(v);}
inline void glTexCoord1( const GLfloat  v[1] ) {glTexCoord1fv(v);}
inline void glTexCoord1( const GLint    v[1] ) {glTexCoord1iv(v);}
inline void glTexCoord1( const GLshort  v[1] ) {glTexCoord1sv(v);}

inline void glTexCoord2( const GLdouble v[2] ) {glTexCoord2dv(v);}
inline void glTexCoord2( const GLfloat  v[2] ) {glTexCoord2fv(v);}
inline void glTexCoord2( const GLint    v[2] ) {glTexCoord2iv(v);}
inline void glTexCoord2( const GLshort  v[2] ) {glTexCoord2sv(v);}

inline void glTexCoord3( const GLdouble v[3] ) {glTexCoord3dv(v);}
inline void glTexCoord3( const GLfloat  v[3] ) {glTexCoord3fv(v);}
inline void glTexCoord3( const GLint    v[3] ) {glTexCoord3iv(v);}
inline void glTexCoord3( const GLshort  v[3] ) {glTexCoord3sv(v);}

inline void glTexCoord4( const GLdouble v[4] ) {glTexCoord4dv(v);}
inline void glTexCoord4( const GLfloat  v[4] ) {glTexCoord4fv(v);}
inline void glTexCoord4( const GLint    v[4] ) {glTexCoord4iv(v);}
inline void glTexCoord4( const GLshort  v[4] ) {glTexCoord4sv(v);}


inline void glRasterPos( GLdouble x, GLdouble y ) {glRasterPos2d(x,y);}
inline void glRasterPos( GLfloat  x, GLfloat  y ) {glRasterPos2f(x,y);}
inline void glRasterPos( GLint    x, GLint    y ) {glRasterPos2i(x,y);}
inline void glRasterPos( GLshort  x, GLshort  y ) {glRasterPos2s(x,y);}

inline void glRasterPos( GLdouble x, GLdouble y, GLdouble z ) {glRasterPos3d(x,y,z);}
inline void glRasterPos( GLfloat  x, GLfloat  y, GLfloat  z ) {glRasterPos3f(x,y,z);}
inline void glRasterPos( GLint    x, GLint    y, GLint    z ) {glRasterPos3i(x,y,z);}
inline void glRasterPos( GLshort  x, GLshort  y, GLshort  z ) {glRasterPos3s(x,y,z);}

inline void glRasterPos( GLdouble x, GLdouble y, GLdouble z, GLdouble w ) {glRasterPos4d(x,y,z,w);}
inline void glRasterPos( GLfloat  x, GLfloat  y, GLfloat  z, GLfloat  w ) {glRasterPos4f(x,y,z,w);}
inline void glRasterPos( GLint    x, GLint    y, GLint    z, GLint    w ) {glRasterPos4i(x,y,z,w);}
inline void glRasterPos( GLshort  x, GLshort  y, GLshort  z, GLshort  w ) {glRasterPos4s(x,y,z,w);}

inline void glRasterPos2( const GLdouble v[2] ) {glRasterPos2dv(v);}
inline void glRasterPos2( const GLfloat  v[2] ) {glRasterPos2fv(v);}
inline void glRasterPos2( const GLint    v[2] ) {glRasterPos2iv(v);}
inline void glRasterPos2( const GLshort  v[2] ) {glRasterPos2sv(v);}

inline void glRasterPos3( const GLdouble v[3] ) {glRasterPos3dv(v);}
inline void glRasterPos3( const GLfloat  v[3] ) {glRasterPos3fv(v);}
inline void glRasterPos3( const GLint    v[3] ) {glRasterPos3iv(v);}
inline void glRasterPos3( const GLshort  v[3] ) {glRasterPos3sv(v);}

inline void glRasterPos4( const GLdouble v[4] ) {glRasterPos4dv(v);}
inline void glRasterPos4( const GLfloat  v[4] ) {glRasterPos4fv(v);}
inline void glRasterPos4( const GLint    v[4] ) {glRasterPos4iv(v);}
inline void glRasterPos4( const GLshort  v[4] ) {glRasterPos4sv(v);}


inline void glRect( GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2 ) {glRectd(x1,y1,x2,y2);}
inline void glRect( GLfloat  x1, GLfloat  y1, GLfloat  x2, GLfloat  y2 ) {glRectf(x1,y1,x2,y2);}
inline void glRect( GLint    x1, GLint    y1, GLint    x2, GLint    y2 ) {glRecti(x1,y1,x2,y2);}
inline void glRect( GLshort  x1, GLshort  y1, GLshort  x2, GLshort  y2 ) {glRects(x1,y1,x2,y2);}


inline void glRect( const GLdouble v1[2], const GLdouble v2[2] ) {glRectdv(v1,v2);}
inline void glRect( const GLfloat  v1[2], const GLfloat  v2[2] ) {glRectfv(v1,v2);}
inline void glRect( const GLint    v1[2], const GLint    v2[2] ) {glRectiv(v1,v2);}
inline void glRect( const GLshort  v1[2], const GLshort  v2[2] ) {glRectsv(v1,v2);}

inline void glLight( GLenum light, GLenum pname, GLfloat         param  ) {glLightf(light,pname,param);}
inline void glLight( GLenum light, GLenum pname, GLint           param  ) {glLighti(light,pname,param);}
inline void glLight( GLenum light, GLenum pname, const GLfloat  *params ) {glLightfv(light,pname,params);}
inline void glLight( GLenum light, GLenum pname, const GLint    *params ) {glLightiv(light,pname,params);}


inline void glGetLight( GLenum light, GLenum pname, GLfloat  *params )
  {glGetLightfv(light,pname,params );}
inline void glGetLight( GLenum light, GLenum pname, GLint    *params )
  {glGetLightiv(light,pname,params );}


inline void glLightModel( GLenum pname, GLfloat  param )
  {glLightModelf(pname,param );}
inline void glLightModel( GLenum pname, GLint    param )
  {glLightModeli(pname,param);}
inline void glLightModel( GLenum pname, const GLfloat  *params )
  {glLightModelfv(pname,params );}
inline void glLightModel( GLenum pname, const GLint    *params )
  {glLightModeliv(pname,params );}

inline void glMaterial( GLenum face, GLenum pname, GLfloat  param )
  {glMaterialf(face,pname,param );}
inline void glMaterial( GLenum face, GLenum pname, GLint    param )
  {glMateriali(face,pname,param );}
inline void glMaterial( GLenum face, GLenum pname, const GLfloat  *params )
  {glMaterialfv(face,pname,params );}
inline void glMaterial( GLenum face, GLenum pname, const GLint    *params )
  {glMaterialiv(face,pname,params );}

#if 0

inline void glGetMaterialfv( GLenum face, GLenum pname, GLfloat  *params )
inline void glGetMaterialiv( GLenum face, GLenum pname, GLint    *params )

inline void glPixelStoref( GLenum pname, GLfloat  param )
inline void glPixelStorei( GLenum pname, GLint    param )

inline void glPixelTransferf( GLenum pname, GLfloat  param )
inline void glPixelTransferi( GLenum pname, GLint    param )

inline void glPixelMapfv( GLenum map, GLint    mapsize,
                                      const GLfloat  *values )
inline void glPixelMapuiv( GLenum map, GLint    mapsize,
                                       const GLuint   *values )
inline void glPixelMapusv( GLenum map, GLint    mapsize,
                                       const GLushort *values )

inline void glGetPixelMapfv( GLenum map, GLfloat  *values )
inline void glGetPixelMapuiv( GLenum map, GLuint   *values )
inline void glGetPixelMapusv( GLenum map, GLushort *values )

inline void glReadPixels( GLint    x, GLint    y,
                                      GLsizei width, GLsizei height,
                                      GLenum format, GLenum type,
                                      GLvoid *pixels )

inline void glDrawPixels( GLsizei width, GLsizei height,
                                      GLenum format, GLenum type,
                                      const GLvoid *pixels )

inline void glCopyPixels( GLint    x, GLint    y,
                                      GLsizei width, GLsizei height,
                                      GLenum type )




inline void glTexGend( GLenum coord, GLenum pname, GLdouble param )
inline void glTexGenf( GLenum coord, GLenum pname, GLfloat  param )
inline void glTexGeni( GLenum coord, GLenum pname, GLint    param )

inline void glTexGendv( GLenum coord, GLenum pname, const GLdouble *params )
inline void glTexGenfv( GLenum coord, GLenum pname, const GLfloat  *params )
inline void glTexGeniv( GLenum coord, GLenum pname, const GLint    *params )

inline void glGetTexGendv( GLenum coord, GLenum pname, GLdouble *params )
inline void glGetTexGenfv( GLenum coord, GLenum pname, GLfloat  *params )
inline void glGetTexGeniv( GLenum coord, GLenum pname, GLint    *params )


inline void glTexEnvf( GLenum target, GLenum pname, GLfloat  param )
inline void glTexEnvi( GLenum target, GLenum pname, GLint    param )

inline void glTexEnvfv( GLenum target, GLenum pname, const GLfloat  *params )
inline void glTexEnviv( GLenum target, GLenum pname, const GLint    *params )

inline void glGetTexEnvfv( GLenum target, GLenum pname, GLfloat  *params )
inline void glGetTexEnviv( GLenum target, GLenum pname, GLint    *params )


inline void glTexParameterf( GLenum target, GLenum pname, GLfloat  param )
inline void glTexParameteri( GLenum target, GLenum pname, GLint    param )

inline void glTexParameterfv( GLenum target, GLenum pname,
                                          const GLfloat  *params )
inline void glTexParameteriv( GLenum target, GLenum pname,
                                          const GLint    *params )

inline void glGetTexParameterfv( GLenum target,
                                             GLenum pname, GLfloat  *params)
inline void glGetTexParameteriv( GLenum target,
                                             GLenum pname, GLint    *params )

inline void glGetTexLevelParameterfv( GLenum target, GLint    level,
                                                  GLenum pname, GLfloat  *params )
inline void glGetTexLevelParameteriv( GLenum target, GLint    level,
                                                  GLenum pname, GLint    *params )


inline void glTexImage1D( GLenum target, GLint    level,
                                      GLint    internalFormat,
                                      GLsizei width, GLint    border,
                                      GLenum format, GLenum type,
                                      const GLvoid *pixels )

inline void glTexImage2D( GLenum target, GLint    level,
                                      GLint    internalFormat,
                                      GLsizei width, GLsizei height,
                                      GLint    border, GLenum format, GLenum type,
                                      const GLvoid *pixels )

inline void glGetTexImage( GLenum target, GLint    level,
                                       GLenum format, GLenum type,
                                       GLvoid *pixels )



inline void glMap1d( GLenum target, GLdouble u1, GLdouble u2,
                                 GLint    stride,
                                 GLint    order, const GLdouble *points )
inline void glMap1f( GLenum target, GLfloat  u1, GLfloat  u2,
                                 GLint    stride,
                                 GLint    order, const GLfloat  *points )

inline void glMap2d( GLenum target,
		     GLdouble u1, GLdouble u2, GLint    ustride, GLint    uorder,
		     GLdouble v1, GLdouble v2, GLint    vstride, GLint    vorder,
		     const GLdouble *points )
inline void glMap2f( GLenum target,
		     GLfloat  u1, GLfloat  u2, GLint    ustride, GLint    uorder,
		     GLfloat  v1, GLfloat  v2, GLint    vstride, GLint    vorder,
		     const GLfloat  *points )

inline void glGetMapdv( GLenum target, GLenum query, GLdouble *v )
inline void glGetMapfv( GLenum target, GLenum query, GLfloat  *v )
inline void glGetMapiv( GLenum target, GLenum query, GLint    *v )

inline void glEvalCoord1d( GLdouble u )
inline void glEvalCoord1f( GLfloat  u )

inline void glEvalCoord1dv( const GLdouble *u )
inline void glEvalCoord1fv( const GLfloat  *u )

inline void glEvalCoord2d( GLdouble u, GLdouble v )
inline void glEvalCoord2f( GLfloat  u, GLfloat  v )

inline void glEvalCoord2dv( const GLdouble *u )
inline void glEvalCoord2fv( const GLfloat  *u )

inline void glMapGrid1d( GLint    un, GLdouble u1, GLdouble u2 )
inline void glMapGrid1f( GLint    un, GLfloat  u1, GLfloat  u2 )

inline void glMapGrid2d( GLint    un, GLdouble u1, GLdouble u2,
                                     GLint    vn, GLdouble v1, GLdouble v2 )
inline void glMapGrid2f( GLint    un, GLfloat  u1, GLfloat  u2,
                                     GLint    vn, GLfloat  v1, GLfloat  v2 )

inline void glEvalPoint1( GLint    i )

inline void glEvalPoint2( GLint    i, GLint    j )

inline void glEvalMesh1( GLenum mode, GLint    i1, GLint    i2 )

inline void glEvalMesh2( GLenum mode, GLint    i1, GLint    i2, GLint    j1, GLint    j2 )



/*
 * Fog
 */

inline void glFogf( GLenum pname, GLfloat  param )

inline void glFogi( GLenum pname, GLint    param )

inline void glFogfv( GLenum pname, const GLfloat  *params )

inline void glFogiv( GLenum pname, const GLint    *params )





inline void glGetColorTableParameterfvEXT( GLenum target,
                                                       GLenum pname,
                                                       GLfloat  *params )

inline void glGetColorTableParameterivEXT( GLenum target,
                                                       GLenum pname,
                                                       GLint    *params )


/* GL_SGIS_multitexture */

inline void glMultiTexCoord1dSGIS(GLenum target, GLdouble s)
inline void glMultiTexCoord1dvSGIS(GLenum target, const GLdouble *v)
inline void glMultiTexCoord1fSGIS(GLenum target, GLfloat  s)
inline void glMultiTexCoord1fvSGIS(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord1iSGIS(GLenum target, GLint    s)
inline void glMultiTexCoord1ivSGIS(GLenum target, const GLint    *v)
inline void glMultiTexCoord1sSGIS(GLenum target, GLshort  s)
inline void glMultiTexCoord1svSGIS(GLenum target, const GLshort  *v)
inline void glMultiTexCoord2dSGIS(GLenum target, GLdouble s, GLdouble t)
inline void glMultiTexCoord2dvSGIS(GLenum target, const GLdouble *v)
inline void glMultiTexCoord2fSGIS(GLenum target, GLfloat  s, GLfloat  t)
inline void glMultiTexCoord2fvSGIS(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord2iSGIS(GLenum target, GLint    s, GLint    t)
inline void glMultiTexCoord2ivSGIS(GLenum target, const GLint    *v)
inline void glMultiTexCoord2sSGIS(GLenum target, GLshort  s, GLshort  t)
inline void glMultiTexCoord2svSGIS(GLenum target, const GLshort  *v)
inline void glMultiTexCoord3dSGIS(GLenum target, GLdouble s, GLdouble t, GLdouble r)
inline void glMultiTexCoord3dvSGIS(GLenum target, const GLdouble *v)
inline void glMultiTexCoord3fSGIS(GLenum target, GLfloat  s, GLfloat  t, GLfloat  r)
inline void glMultiTexCoord3fvSGIS(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord3iSGIS(GLenum target, GLint    s, GLint    t, GLint    r)
inline void glMultiTexCoord3ivSGIS(GLenum target, const GLint    *v)
inline void glMultiTexCoord3sSGIS(GLenum target, GLshort  s, GLshort  t, GLshort  r)
inline void glMultiTexCoord3svSGIS(GLenum target, const GLshort  *v)
inline void glMultiTexCoord4dSGIS(GLenum target, GLdouble s, GLdouble t, GLdouble r, GLdouble q)
inline void glMultiTexCoord4dvSGIS(GLenum target, const GLdouble *v)
inline void glMultiTexCoord4fSGIS(GLenum target, GLfloat  s, GLfloat  t, GLfloat  r, GLfloat  q)
inline void glMultiTexCoord4fvSGIS(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord4iSGIS(GLenum target, GLint    s, GLint    t, GLint    r, GLint    q)
inline void glMultiTexCoord4ivSGIS(GLenum target, const GLint    *v)
inline void glMultiTexCoord4sSGIS(GLenum target, GLshort  s, GLshort  t, GLshort  r, GLshort  q)
inline void glMultiTexCoord4svSGIS(GLenum target, const GLshort  *v)

inline void glMultiTexCoordPointerSGIS(GLenum target, GLint    size, GLenum type, GLsizei stride, const GLvoid *pointer)

inline void glSelectTextureSGIS(GLenum target)

inline void glSelectTextureCoordSetSGIS(GLenum target)


/* GL_EXT_multitexture */

inline void glMultiTexCoord1dEXT(GLenum target, GLdouble s)
inline void glMultiTexCoord1dvEXT(GLenum target, const GLdouble *v)
inline void glMultiTexCoord1fEXT(GLenum target, GLfloat  s)
inline void glMultiTexCoord1fvEXT(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord1iEXT(GLenum target, GLint    s)
inline void glMultiTexCoord1ivEXT(GLenum target, const GLint    *v)
inline void glMultiTexCoord1sEXT(GLenum target, GLshort  s)
inline void glMultiTexCoord1svEXT(GLenum target, const GLshort  *v)
inline void glMultiTexCoord2dEXT(GLenum target, GLdouble s, GLdouble t)
inline void glMultiTexCoord2dvEXT(GLenum target, const GLdouble *v)
inline void glMultiTexCoord2fEXT(GLenum target, GLfloat  s, GLfloat  t)
inline void glMultiTexCoord2fvEXT(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord2iEXT(GLenum target, GLint    s, GLint    t)
inline void glMultiTexCoord2ivEXT(GLenum target, const GLint    *v)
inline void glMultiTexCoord2sEXT(GLenum target, GLshort  s, GLshort  t)
inline void glMultiTexCoord2svEXT(GLenum target, const GLshort  *v)
inline void glMultiTexCoord3dEXT(GLenum target, GLdouble s, GLdouble t, GLdouble r)
inline void glMultiTexCoord3dvEXT(GLenum target, const GLdouble *v)
inline void glMultiTexCoord3fEXT(GLenum target, GLfloat  s, GLfloat  t, GLfloat  r)
inline void glMultiTexCoord3fvEXT(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord3iEXT(GLenum target, GLint    s, GLint    t, GLint    r)
inline void glMultiTexCoord3ivEXT(GLenum target, const GLint    *v)
inline void glMultiTexCoord3sEXT(GLenum target, GLshort  s, GLshort  t, GLshort  r)
inline void glMultiTexCoord3svEXT(GLenum target, const GLshort  *v)
inline void glMultiTexCoord4dEXT(GLenum target, GLdouble s, GLdouble t, GLdouble r, GLdouble q)
inline void glMultiTexCoord4dvEXT(GLenum target, const GLdouble *v)
inline void glMultiTexCoord4fEXT(GLenum target, GLfloat  s, GLfloat  t, GLfloat  r, GLfloat  q)
inline void glMultiTexCoord4fvEXT(GLenum target, const GLfloat  *v)
inline void glMultiTexCoord4iEXT(GLenum target, GLint    s, GLint    t, GLint    r, GLint    q)
inline void glMultiTexCoord4ivEXT(GLenum target, const GLint    *v)
inline void glMultiTexCoord4sEXT(GLenum target, GLshort  s, GLshort  t, GLshort  r, GLshort  q)
inline void glMultiTexCoord4svEXT(GLenum target, const GLshort  *v)

inline void glInterleavedTextureCoordSetsEXT( GLint    factor )

inline void glSelectTextureEXT( GLenum target )

inline void glSelectTextureCoordSetEXT( GLenum target )

inline void glSelectTextureTransformEXT( GLenum target )






/* GL_EXT_point_parameters */
inline void glPointParameterfEXT( GLenum pname, GLfloat  param )
inline void glPointParameterfvEXT( GLenum pname,
                                               const GLfloat  *params )



#endif

#endif
