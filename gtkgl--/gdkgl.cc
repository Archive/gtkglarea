// This is -*- C++ -*-

/*
 * gdkgl.cc
 *
 * Copyright 1999 Karl E. Nelson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <gtkgl--/gdkgl.h>
         
bool Gdk_GL::query()
  {return gdk_gl_query();}

Gdk_Visual Gdk_GL::choose_visual(int *attrlist)
  {return gdk_gl_choose_visual(attrlist);}

void Gdk_GL::use_font(const Gdk_Font& font,
              int first,
              int count,
              int list_base)
  {gdk_gl_use_gdk_font(gdk_const_cast(font),first,count,list_base);}

void Gdk_GL::wait_gdk()
  {gdk_gl_wait_gdk();}

void Gdk_GL::wait_gl()
  {gdk_gl_wait_gl();}

void Gdk_GL::swap_buffers(Gdk_Drawable& d)
  {gdk_gl_swap_buffers(d);}


/**************************************************************/
  
void Gdk_GL::Context::ref()
  {
   if (obj_)  
     gdk_gl_context_ref(obj_);
  }

void Gdk_GL::Context::unref()
  {
   if (obj_) 
     gdk_gl_context_unref(obj_);
   obj_=0;
  }

Gdk_GL::Context::Context()
  :Gdk_Handle<GdkGLContext>(0)
  {}

Gdk_GL::Context::Context(const Gdk_Visual &visual)
  :Gdk_Handle<GdkGLContext>(0)
  {create(visual);}

Gdk_GL::Context::Context(const Gdk_Visual &visual,
                        Context &share,
                        bool direct=FALSE)
  :Gdk_Handle<GdkGLContext>(0)
  {create(visual,share,direct);}

Gdk_GL::Context::        Context(GdkGLContext* context)
  :Gdk_Handle<GdkGLContext>(context)
  {
   ref();
  }

Gdk_GL::Context::Context(const Context& context)
  :Gdk_Handle<GdkGLContext>(0)
  {
   obj_=context.obj_;
   ref();
  }
        
Gdk_GL::Context::~Context()
  {
   unref();
  }
       
void Gdk_GL::Context::create(const Gdk_Visual &visual)
  {
   unref();
   obj_ = gdk_gl_context_new(gdk_const_cast(visual));
   ref();
  } 

void Gdk_GL::Context::create(const Gdk_Visual &visual,Context &share,bool direct=FALSE)
  {
   unref();
   obj_ = gdk_gl_context_share_new(gdk_const_cast(visual),share,direct);
   ref();
  } 

void Gdk_GL::Context::release()
  {unref(); }

gint Gdk_GL::Context::make_current(Gdk_Drawable &d)
  {return gdk_gl_make_current(d,*this);}

gint Gdk_GL::Context::make_current(Pixmap       &d)
  {return gdk_gl_pixmap_make_current(d,*this);}

/**************************************************************/

void Gdk_GL::Pixmap::ref()
  {   
   if (obj_)  
     gdk_gl_pixmap_ref(obj_);
  }

void Gdk_GL::Pixmap::unref()
  {
   if (obj_) 
     gdk_gl_pixmap_unref(obj_);
   obj_=0;
  } 

Gdk_GL::Pixmap::Pixmap()
  :Gdk_Handle<GdkGLPixmap>(0)
          {}

Gdk_GL::Pixmap::Pixmap(const Gdk_Visual& visual,
       gint       width,
       gint       height)
  :Gdk_Handle<GdkGLPixmap>(0)
  {create(visual,width,height);}

Gdk_GL::Pixmap::Pixmap(const Gdk_Visual& visual,Gdk_Pixmap& pixmap)
  :Gdk_Handle<GdkGLPixmap>(0)
  {create(visual,pixmap);}

Gdk_GL::Pixmap::Pixmap(const Pixmap& p)
  :Gdk_Handle<GdkGLPixmap>(p)
  {
   ref();
  }

Gdk_GL::Pixmap::Pixmap(GdkGLPixmap *p)
  :Gdk_Handle<GdkGLPixmap>(p)
  {
   ref();
  }

void Gdk_GL::Pixmap::create(const Gdk_Visual& visual,Gdk_Pixmap& pixmap)
  {
   unref();
   pixmap_=pixmap;
   if (!pixmap_.connected()) return;
   obj_=gdk_gl_pixmap_new(gdk_const_cast(visual),pixmap_);
   ref();
  }

void Gdk_GL::Pixmap::create(const Gdk_Visual& visual,gint width,gint height)
  {
   unref();
   if (!visual.connected()) return;
   pixmap_.create(width,height,
   ((GdkVisual*)gdk_const_cast(visual))->depth);
   obj_=gdk_gl_pixmap_new(gdk_const_cast(visual),pixmap_);
   ref();
  } 

void Gdk_GL::Pixmap::release()
  {
   unref();
   pixmap_.release();
  }
