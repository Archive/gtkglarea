// This is -*- C++ -*-
#ifndef __GDKMM_GL_H_
#define __GDKMM_GL_H_
/*
 * gdkgl.h
 *
 * Copyright 1999 Karl E. Nelson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <gtkgl/gdkgl.h>
#include <gdk--/types.h>
#include <gdk--/pixmap.h>
#include <gdk--/visual.h>
#include <gdk--/font.h>

namespace Gdk_GL {
struct CONFIGS
  {
    static const int NONE 		=GDK_GL_NONE;
    static const int USE_GL 	=GDK_GL_USE_GL;
    static const int BUFFER_SIZE 	=GDK_GL_BUFFER_SIZE;
    static const int LEVEL 		=GDK_GL_LEVEL;
    static const int RGBA 		=GDK_GL_RGBA;
    static const int DOUBLEBUFFER 	=GDK_GL_DOUBLEBUFFER;
    static const int STEREO 	=GDK_GL_STEREO;
    static const int AUX_BUFFERS 	=GDK_GL_AUX_BUFFERS;
    static const int RED_SIZE 	=GDK_GL_RED_SIZE;
    static const int GREEN_SIZE 	=GDK_GL_GREEN_SIZE;
    static const int BLUE_SIZE 	=GDK_GL_BLUE_SIZE;
    static const int ALPHA_SIZE 	=GDK_GL_ALPHA_SIZE;
    static const int DEPTH_SIZE 	=GDK_GL_DEPTH_SIZE;
    static const int STENCIL_SIZE 	=GDK_GL_STENCIL_SIZE;
    static const int ACCUM_RED_SIZE 	=GDK_GL_ACCUM_RED_SIZE;
    static const int ACCUM_GREEN_SIZE 	=GDK_GL_ACCUM_GREEN_SIZE;
    static const int ACCUM_BLUE_SIZE 	=GDK_GL_ACCUM_BLUE_SIZE;
    static const int ACCUM_ALPHA_SIZE 	=GDK_GL_ACCUM_ALPHA_SIZE;

    /* GLX_EXT_visual_info extension */
    struct EXT
      {
        static const int X_VISUAL_TYPE =GDK_GL_X_VISUAL_TYPE_EXT;
        static const int TRANSPARENT_TYPE =GDK_GL_TRANSPARENT_TYPE_EXT;
        static const int TRANSPARENT_INDEX_VALUE =GDK_GL_TRANSPARENT_INDEX_VALUE_EXT;
        static const int TRANSPARENT_RED_VALUE =GDK_GL_TRANSPARENT_RED_VALUE_EXT;
        static const int TRANSPARENT_GREEN_VALUE =GDK_GL_TRANSPARENT_GREEN_VALUE_EXT;
        static const int TRANSPARENT_BLUE_VALUE =GDK_GL_TRANSPARENT_BLUE_VALUE_EXT;
        static const int TRANSPARENT_ALPHA_VALUE =GDK_GL_TRANSPARENT_ALPHA_VALUE_EXT;
      };
  };

// Checks if OpenGL is available.          
bool query();

Gdk_Visual choose_visual(int *attrlist);
void use_font(const Gdk_Font& font,
                          int first,
                          int count,
                          int list_base);
void wait_gdk();
void wait_gl();
void swap_buffers(Gdk_Drawable& d);
  
class Pixmap; 
class Context: public Gdk_Handle<GdkGLContext>
  {
    private:
      virtual void ref();
      virtual void unref();

#ifdef GDKMM_HANDLES_CONNECTED_ONLY
    protected:
#else
    public:
#endif
      Context();
    public:
      Context(const Gdk_Visual &visual);
      Context(const Gdk_Visual &visual,Context &share,bool direct=FALSE);
      Context(GdkGLContext* context);
      Context(const Context& context);
      ~Context();
       
      void create(const Gdk_Visual &visual);
      void create(const Gdk_Visual &visual,Context &share,bool direct=FALSE);

      void release();

      gint make_current(Gdk_Drawable &d);
      gint make_current(Pixmap       &d);
   };

class Pixmap: public Gdk_Handle<GdkGLPixmap>
  {
    private:
      Gdk_Pixmap pixmap_;
      virtual void ref();
      virtual void unref();

#ifdef GDKMM_HANDLES_CONNECTED_ONLY
    protected:
#else
    public:
#endif
      Pixmap();
    public:
      // create a new pixmap 
      Pixmap(const Gdk_Visual& visual,
             gint       width,
             gint       height);

      // use a gdk pixmap for gl one
      Pixmap(const Gdk_Visual& visual,Gdk_Pixmap& pixmap);

      // copy existing one
      Pixmap(const Pixmap& p);

      // wrap an existing one
      Pixmap(GdkGLPixmap *p);

      // free resources
      void release();

      // create a gl pixmap from a gdk one
      void create(const Gdk_Visual& visual,Gdk_Pixmap& pixmap);

      // create a gl pixmap 
      void create(const Gdk_Visual& visual,gint width,gint height);

      // convert to a pixmap
      operator Gdk_Pixmap()
        {return pixmap_;}

      // convert to a gdk pixmap
      operator GdkPixmap*()
        {return pixmap_;}
  };

}

typedef Gdk_GL::Context Gdk_GLContext;
typedef Gdk_GL::Pixmap  Gdk_GLPixmap;
#endif
