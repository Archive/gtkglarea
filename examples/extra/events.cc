/*********************************************************************
  Copyright 1998 Drake Diedrich <Drake.Diedrich@anu.edu.au>
 
  This is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*********************************************************************/

#include <string>
#include <iostream.h>
#include <GL/glx.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gtk--.h>
#include <gtkgl--/glarea.h>
#include <GL/gl.h>
#include <GL/glu.h>


void gl() {
  static int displaylist=-1;
  if (displaylist<0) {
    //  cerr << "gl() compile & execute" << endl;
    displaylist=glGenLists(1);
    glNewList(displaylist,GL_COMPILE_AND_EXECUTE);
    glPushMatrix();
    glTranslatef(0,0,-10);
    glColor3f(1.0,0.5,0.2);
    GLUquadricObj *quad = gluNewQuadric();
    gluSphere(quad,1,10,6);
    gluDeleteQuadric(quad);
    glPopMatrix();
    glEndList();
  } else {
    // cerr << "gl() displaylist" << endl;
    glCallList(displaylist);
  }
}

class MyGLArea : public Gtk::GLArea {
public:
  string name;

  gint draw(GdkEventExpose* expose) {
    if (expose->count>0) {
      cerr << "draw() " << name << " count>0" << endl;
      return 1;
    }
    cerr << "draw() " << name << endl;
    if (make_current()) {
      glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );    
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      gl();
    }
    swap_buffers(); 
    return 1;
  }

  void initgl() {
    //  cerr << "initgl() " << name << endl;
    if (make_current()) {
      glViewport(0,0,width(),height());
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluPerspective(30.0, ((double)width()/height()), 1.0,200.0);
      glClearColor(0,0,0,0);
    }
  }

  MyGLArea() : Gtk::GLArea() {
    // cerr << "MyGLArea()" << endl;
    expose_event.connect(slot(this,&MyGLArea::draw));
    realize.connect(slot(this,&MyGLArea::initgl));
    set_events(GDK_EXPOSURE_MASK);
    set_usize(400,300);
  };

  MyGLArea(MyGLArea &share) :
    Gtk::GLArea(Gtk::GLAREA_DEFAULT_ATTR,share) {
    //  cerr << "MyGLArea(" << share.name << ")" << endl;
    expose_event.connect(slot(this,&MyGLArea::draw));
    realize.connect(slot(this,&MyGLArea::initgl));
    set_events(GDK_EXPOSURE_MASK);
    set_usize(400,300);
  };
};


int main(int argc,char **argv) {
  Gtk::Main app(argc,argv);
  Gtk::Window left,right;
  MyGLArea area1;

  area1.name = "left";
  area1.show();
  left.add(area1);
  left.set_title("Left");
  left.show();

  MyGLArea area2(area1);
  area2.name = "right";
  area2.show();
  right.add(area2);
  right.set_title("Right");
  right.show();

  app.run();
  return 0;
}

