/*********************************************************************
  Copyright 1998 Karl Nelson <kenelson@ece.ucdavis.edu>
 
  This is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*********************************************************************/
#include <iostream.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gtk--.h>

#include <gtkgl--/glarea.h>

// WARNING: Verbosity level set to maximum, prepare to get an ear full

/*********************************************************************
***** Object_Display
**********************************************************************
  This class will be our OpenGL widget of three geometric objects
*********************************************************************/
class Object_Display:public Gtk::GLArea
  {private:
     GLint cube,sphere,cylinder;
     bool flag_cube,flag_sphere,flag_cylinder;
     
     void draw_cube();
     void draw_sphere();
     void draw_cylinder();
 
     void initgl();
     void setupgl();
     void setup_pick(gdouble x, gdouble y);
     void drawgl();
    
   public:
     Object_Display();
     ~Object_Display();

     // take over the impl functions for events 
     virtual void realize_impl();
     virtual gint expose_event_impl(GdkEventExpose* expose);
     virtual gint configure_event_impl(GdkEventConfigure *event);
     virtual gint button_press_event_impl(GdkEventButton*); 
 
     // callbacks we will connect to other widgets
     void toggle_cube(Gtk::ToggleButton *button);
     void toggle_sphere(Gtk::ToggleButton *button);
     void toggle_cylinder(Gtk::ToggleButton *button);

     // signals to update buttons 
     SigC::Signal1<void,bool>set_cube;
     SigC::Signal1<void,bool>set_sphere;
     SigC::Signal1<void,bool>set_cylinder;
  };

/***********************************************************
***** Object_Display::Object_Display
************************************************************
  This constructor makes the connections necessary to
  handle exposures and to initialize the GL object.
  
  Note: We can NOT constuct or execute any GL commands
  at this point because we have not yet been realized.
  Therefore, initialization of the GL odjects must be
  done in a seperate realization function.  In this
  case we will connect it to initgl().

  (the show signal happens before realization and should 
   not be used.)
***********************************************************/
Object_Display::Object_Display():Gtk::GLArea() 
  {
   cube=sphere=cylinder=0;
   flag_cube=flag_sphere=flag_cylinder=0;

   //select events so that we can properly connect to them
     //GDK_POINTER_MOTION_HINT_MASK is used so that we 
     //don't get every point of motion
   set_events(GDK_EXPOSURE_MASK|
              GDK_BUTTON_PRESS_MASK);
  }


void Object_Display::realize_impl()
  {
   // we must call the real realize_impl so the object is 
   // marked realized
   Gtk::GLArea::realize_impl();

   // then we can call our init
   initgl();
  }

gint Object_Display::expose_event_impl(GdkEventExpose *event)
  {
   // we are CPU intensive so we should wait for the last event
   if (event->count>0) return TRUE;
   drawgl();
   return TRUE;
  }

gint Object_Display::configure_event_impl(GdkEventConfigure *event)
  {
   if (make_current())
     setupgl();
   return TRUE;
  }


// Function has not been test (as far as I know)
Object_Display::~Object_Display()
  {
   if (glIsList(cube)==GL_TRUE)
     glDeleteLists(cube,1);
   if (glIsList(sphere)==GL_TRUE)
     glDeleteLists(sphere,1);
   if (glIsList(cylinder)==GL_TRUE)
     glDeleteLists(cylinder,1);
  }

/***********************************************************
***** Object_Display::initgl
************************************************************
  This function creates the OpenGL list that will be used 
  to draw the planet.  We cannot do any GL rendering until
  the widget is realized including creating lists.
***********************************************************/
void Object_Display::initgl()
  {GLUquadricObj *obj;

   // In order to allow the window to be shrunk we will 
   // reset the minumum size to something smaller now 
   // that the inital allocation has been completed.
   //  (this works, but is this what they intended??)
   set_usize(100,100);

   // make_current must be called before performing GL operations
   // and GL code should only be called if make_current returns true
   if (make_current())
     {
      setupgl();

      // I will use glu functions to generate the objects, but
      // lists to display them later (probably not the norm
      // but it is more understandable)

      cube = glGenLists(1);
      glNewList(cube, GL_COMPILE);
      glShadeModel(GL_FLAT);
      glFrontFace(GL_CCW);
      glBegin(GL_QUAD_STRIP);
        glNormal3f(0,0,1);
        glVertex3f(1,1,1);
        glVertex3f(-1,1,1);
        glVertex3f(1,-1,1);
        glVertex3f(-1,-1,1);

        glNormal3f(0,-1,0);
        glVertex3f(1,-1,-1);
        glVertex3f(-1,-1,-1);

        glNormal3f(0,0,-1);
        glVertex3f(1,1,-1);
        glVertex3f(-1,1,-1);

        glNormal3f(0,1,0);
        glVertex3f(1,1,1);
        glVertex3f(-1,1,1);
      glEnd();
      glBegin(GL_QUADS);
        glNormal3f(1,0,0);
        glVertex3f(1,1,1);
        glVertex3f(1,-1,1);
        glVertex3f(1,-1,-1);
        glVertex3f(1,1,-1);

        glNormal3f(-1,0,0);
        glVertex3f(-1,1,1);
        glVertex3f(-1,1,-1);
        glVertex3f(-1,-1,-1);
        glVertex3f(-1,-1,1);
      glEnd();
      glEndList();

      sphere = glGenLists(1);
      glNewList(sphere, GL_COMPILE);
      obj=gluNewQuadric();
      glShadeModel(GL_SMOOTH);
      gluSphere(obj,1.5,20,20);
      gluDeleteQuadric(obj);
      glEndList();

      cylinder = glGenLists(1);
      glNewList(cylinder, GL_COMPILE);
      obj=gluNewQuadric();
      glShadeModel(GL_SMOOTH);
      gluCylinder(obj,1.3,1.3,2.0,15,1);
      glShadeModel(GL_FLAT);
      gluCylinder(obj,0,1.3,0.05,15,1);
      gluDeleteQuadric(obj);
      glEndList();
     }
  }

/***********************************************************
***** Object_Display::draw*
************************************************************
  These functions draw the objects
***********************************************************/
void Object_Display::draw_cube()
  {
   glPushMatrix();
     glTranslatef(-2.0,2.0,0);
     glRotatef(45,0,1,0);
     glRotatef(40,1,0,0);
     glCallList(cube);
   glPopMatrix();
  }

void Object_Display::draw_sphere()
  {
   glPushMatrix();
     glTranslatef(2.0,2.0,0);
     glCallList(sphere);
   glPopMatrix();
  }

void Object_Display::draw_cylinder()
  {
   glPushMatrix();
     glTranslatef(0,-2.0/1.4,0);
     glRotatef(90,1,0,0);
     glCallList(cylinder);
   glPopMatrix();
  }

/***********************************************************
***** Object_Display::setupgl
************************************************************
  This function sets up the projection matrix and
  configures the OpenGL rendering engine. 

  This is called once to set up the initial matrix and
  then again if the window is resized.
***********************************************************/
void Object_Display::setupgl()
  {
   static GLfloat pos[4] = {5.0, 5.0, 10.0, 0.0 };

   // Setup the rendering engine
   glClearColor(0.0, 0.0, 0.0, 0.0); // frame buffer clears should be to black
   glLightfv( GL_LIGHT0, GL_POSITION, pos );
   glEnable( GL_CULL_FACE );
   glEnable( GL_LIGHTING );
   glEnable( GL_LIGHT0 );
   glEnable( GL_DEPTH_TEST );
   glEnable( GL_NORMALIZE );
   
   
   glViewport(0, 0, (GLint)(width()), (GLint)(height()));

   // Create the projection matrix
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   if (width()>height()) 
     {
      GLfloat w = (GLfloat) width() / (GLfloat) height();
      glFrustum( -w, w, -1.0, 1.0, 2.0, 20.0 );
     }
    else 
     {
      GLfloat h = (GLfloat) height() / (GLfloat) width();
      glFrustum( -1.0, 1.0, -h, h, 2.0, 20.0 );
     }

   // Go to the place to start the rendering
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glTranslatef( 0.0, 0.0, -10.0 );

  }

/***********************************************************
***** Object_Display::setup_pick
************************************************************
  This function sets up the projection matrix for a pick
  operation.  It is exactly the same as setupgl (without
  the rendering commands) and with the edition of 
  selecting the viewport to be just arround the cursor.
***********************************************************/
void Object_Display::setup_pick(gdouble x,gdouble y)
  {
   GLint viewport[4];

   glViewport(0, 0, (GLint)(width()), (GLint)(height()));
   glGetIntegerv(GL_VIEWPORT,viewport);

   // Create the projection matrix
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPickMatrix(x, height()-y, 4, 4, viewport);
   if (width()>height()) 
     {
      GLfloat w = (GLfloat) width() / (GLfloat) height();
      glFrustum( -w, w, -1.0, 1.0, 2.0, 20.0 );
     }
    else 
     {
      GLfloat h = (GLfloat) height() / (GLfloat) width();
      glFrustum( -1.0, 1.0, -h, h, 2.0, 20.0 );
     }

   // Go to the place to start the rendering
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glTranslatef( 0.0, 0.0, -10.0 );
  }

/***********************************************************
***** Object_Display::drawgl
************************************************************
  This function just calls the OpenGL list to display the
  object and then commits the rendering scene.
***********************************************************/
void Object_Display::drawgl() 
  {static GLfloat white[]={1.0,1.0,1.0,1.0};
   static GLfloat blue[]= {0.3,0.3,1.0,1.0};
   static GLfloat green[]={0.3,1.0,0.3,1.0};
   static GLfloat red[]=  {1.0,0.3,0.3,1.0};

   // start opengl rendering, you must put all opengl calls
   // inside make_current
   if (make_current())
     {
      glMatrixMode(GL_MODELVIEW);
      glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
      glPushMatrix();

        if (flag_cube)
          glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, blue );
         else
          glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, white );
        draw_cube();
  
        if (flag_sphere)
          glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, green );
         else
          glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, white );
        draw_sphere();

        if (flag_cylinder)
          glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, red );
         else
          glMaterialfv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, white );
        draw_cylinder();

      glPopMatrix();
     }
   
   swap_buffers();
  }

/***********************************************************
***** Object_Display::press
************************************************************
  This function just calls the OpenGL list, but this time
  with a pick operation.  This results in choosing one of the
  shapes.  (Nothing is actually rendered in the pick,
  operation it just fakes it.)

  We do exactly the same thing as to draw the scene, but
  this time we name the objects.
  
  (Code largely taken from OpenGL man pages) 
  
  Two useful debugging lines I don't want to lose. (Checks for 
  stack growth) 
  glGetIntegerv(GL_MODELVIEW_STACK_DEPTH,&i); cout << "Model: "<< i<<endl;
  glGetIntegerv(GL_PROJECTION_STACK_DEPTH,&i); cout << "Proj: "<< i<<endl;
***********************************************************/
gint Object_Display::button_press_event_impl(GdkEventButton *event) 
  {GLuint selectBuf[12];
   int i,hits=0;
   if (make_current())
     {
      // set up the selection buffer (4 ints per hit)
      glSelectBuffer(12, selectBuf);

      // change the render mode to selection and clear the name buffer
      glRenderMode(GL_SELECT);
      glInitNames();
      glPushName(0); // must have something on stack before callins glLoadName

      // so that we don't lose our projection/model matrix, push both
      glMatrixMode(GL_PROJECTION); glPushMatrix();
      glMatrixMode(GL_MODELVIEW); glPushMatrix();
       
        // this will set up the viewport for the selection 
        setup_pick(event->x,event->y);

        // name then draw each object.
        glLoadName(1);
        draw_cube();

        glLoadName(2);
        draw_sphere();

        glLoadName(3);
        draw_cylinder();

      // pop the projection/model matrix
      glMatrixMode(GL_PROJECTION); glPopMatrix();
      glMatrixMode(GL_MODELVIEW); glPopMatrix();

      // switch to render mode, the return value is the number of hits.
      hits=glRenderMode(GL_RENDER);
     }

   // process the selection buffer
   if (hits>0)
     {for (i=0;i<hits;i++)
        {
         switch (selectBuf[(i*4)+3])
           {case 1: set_cube(flag_cube=!flag_cube); 
                    break;
            case 2: set_sphere(flag_sphere=!flag_sphere); 
                    break;
            case 3: set_cylinder(flag_cylinder=!flag_cylinder);
                    break;
           }
        }
    //  drawgl();
     }

   return TRUE;
  }

// call backs that we will connect the buttons to.
void Object_Display::toggle_cube(Gtk::ToggleButton *button)
  {flag_cube=button->get_active();
   drawgl();
  }

void Object_Display::toggle_sphere(Gtk::ToggleButton *button)
  {flag_sphere=button->get_active();
   drawgl();
  }

void Object_Display::toggle_cylinder(Gtk::ToggleButton *button)
  {flag_cylinder=button->get_active();
   drawgl();
  }

/*********************************************************************
***** MainWindow
**********************************************************************
  This class will be our MainWindow containing an OpenGL window 
  and a toolbox to manipulate the OpenGL object. 
*********************************************************************/
class MainWindowClass : public Gtk::Window 
  {private:
     // Pointers to all of the widgets
     Gtk::Button *button;
     Gtk::ToggleButton *button_cube;
     Gtk::ToggleButton *button_sphere;
     Gtk::ToggleButton *button_cylinder;
     Gtk::Frame *frame;
     Gtk::Table *table;
     Object_Display *display;
   public:
      MainWindowClass (void);
      ~MainWindowClass (void);
      gint delete_event_impl(GdkEventAny *) 
        {Gtk::Main::quit();
         return 0;
        }
  };

/***********************************************************
***** MainWindowClass::MainWindowClass
************************************************************
  Creates the main window and all of its widgets in a 
  bottom approach.  (I just find the style more readable
  than top down.)  View the file layout.ps for a description
  of the widget relations.
***********************************************************/
MainWindowClass::MainWindowClass(void) : Gtk::Window(GTK_WINDOW_TOPLEVEL)
  {
   set_title("Pick Me");
 
   // Create the OpenGL object (the whole point of this exercise) 
   display = new Object_Display();
   display->set_usize(300,300);
   display->show();

   // Gtk::GLArea can't have a border, so we will stick it in a frame
   frame = new Gtk::Frame();
   frame->set_shadow_type (GTK_SHADOW_IN);
   frame->set_border_width(10);  
   frame->add(*display);   
   frame->show();

   // Create a Quit button
   button_cube = new Gtk::ToggleButton("Cube");
   button_cube->set_usize(100,50);
   button_cube->set_border_width(5);
   button_cube->show();

   button_sphere = new Gtk::ToggleButton("Sphere");
   button_sphere->set_usize(100,50);
   button_sphere->set_border_width(5);
   button_sphere->show();

   button_cylinder = new Gtk::ToggleButton("Cylinder");
   button_cylinder->set_usize(100,50);
   button_cylinder->set_border_width(5);
   button_cylinder->show();

   // Create a Quit button
   button = new Gtk::Button("Quit");
   button->set_usize(100,50);
   button->set_border_width(10);
   button->show();

   // Place the Scrollbars and Button in a Table
   // I was using attach_defaults, but it is better if just the scrollbars 
   // grow, to fill the space.
   table = new Gtk::Table(2,4,FALSE);
   table->attach(*frame,0,1,0,4,GTK_EXPAND|GTK_FILL);
   table->attach(*button_cube,1,2,0,1,GTK_SHRINK);
   table->attach(*button_sphere,1,2,1,2,GTK_SHRINK);
   table->attach(*button_cylinder,1,2,2,3,GTK_SHRINK);
   table->attach(*button,1,2,3,4,GTK_SHRINK,GTK_SHRINK,0,0);
   table->set_col_spacing(0,10);
   table->set_row_spacing(2,10);
   table->set_border_width(10);
   table->show();

   // Add the box to the mainwindow
   add(*table);

   // Connect the Quit button to the application quit
   button->clicked.connect(Gtk::Main::quit.slot());

   // Connect buttons to the OpenGL object
   button_cube->toggled
      .connect(bind(slot(display,&Object_Display::toggle_cube),
                    button_cube));
   button_sphere->toggled
      .connect(bind(slot(display,&Object_Display::toggle_sphere),
                    button_sphere));
   button_cylinder->toggled
      .connect(bind(slot(display,&Object_Display::toggle_cylinder),
                    button_cylinder));
 
   // Connect the OpenGL object back to the buttons 
   display->set_cube    .connect(slot(button_cube,
                                      &Gtk::ToggleButton::set_active));
   display->set_sphere  .connect(slot(button_sphere,
                                      &Gtk::ToggleButton::set_active));
   display->set_cylinder.connect(slot(button_cylinder,
                                      &Gtk::ToggleButton::set_active));
  }

MainWindowClass::~MainWindowClass(void)
  {
   delete button;
   delete button_cube;
   delete button_sphere;
   delete button_cylinder;
   delete frame;
   delete table;
   delete display;
  };

/***********************************************************
***** Main 
************************************************************
  very simple main body
***********************************************************/
gint main(gint argc, gchar **argv)
  {
   MainWindowClass *main_window;
  
   Gtk::Main app(argc,argv);

   main_window = new MainWindowClass;
   main_window->show();
  
   Gtk::Main::run();

   return(0);
  }

