/*********************************************************************
  Copyright 1998 Karl Nelson <kenelson@ece.ucdavis.edu>
 
  This is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*********************************************************************/

#ifndef _GLOBE_GL_
#define _GLOBE_GL_
/*********************************************************************
***** globeGL
**********************************************************************
  This draws a nice texture mapped globe made of triangles.
  
  For best results: the number of equator_belts should be between
    the number of pole_belts and twice the number of pole_belts.
    The number of pole_sectors should be between 4 and 7.
    The number of belt sectors should be less then 20.

  The number of sectors arround the center is the number of
  pole_sectors times the pole_belts.  The total number of 
  faces on the object is approx to 
    pole_belts*pole_sectors*(equator_belts+pole_belts)

*********************************************************************/
#define MAX_GLOBE_SECTORS 200

int globeGL(float sphere_radius,int pole_sectors,
                int pole_belts,int equator_belts,int *texture,int rows);

#endif


