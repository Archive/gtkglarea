/*********************************************************************
  Copyright 1998 Karl Nelson <kenelson@ece.ucdavis.edu>
 
  This is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*********************************************************************/
#include <iostream.h>
#include <GL/glx.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gtk--.h>
#include <gtkgl--/glarea.h>

#include <GL/gl.h>
#include <math.h>
#include "globeGL.hh"

// WARNING: Verbosity level set to maximum, prepare to get an ear full

/*********************************************************************
***** Planet_Display
**********************************************************************
  This class will be our OpenGL widget a simple texture mapped
  planet.
*********************************************************************/
class Planet_Display:public Gtk::GLArea
  {
    GLint planet;
    GLfloat xAngle,yAngle,zAngle;
    GLfloat zRot;

    void initgl();
    void setupgl();
    void drawgl();

    gdouble basex,basey;

    // take over some impl methods
    virtual void realize_impl();
    virtual gint motion_notify_event_impl(GdkEventMotion*); 
    virtual gint button_press_event_impl(GdkEventButton*); 
    virtual gint button_release_event_impl(GdkEventButton*); 
    virtual gint expose_event_impl(GdkEventExpose* expose);
    virtual gint configure_event_impl(GdkEventConfigure *event);
    
  public:
    Planet_Display();
    ~Planet_Display();

    // callbacks we will connect to other widgets
    void setx(Gtk::Adjustment *);
    void sety(Gtk::Adjustment *);
    void setz(Gtk::Adjustment *);
    int idle();

    // signals to update scrollbars 
    SigC::Signal1<void,gfloat>updatex;
    SigC::Signal1<void,gfloat>updatey;
    SigC::Signal1<void,gfloat>updatez;
  };

/***********************************************************
***** Planet_Display::Planet_Display
************************************************************
  This constructor makes the connections necessary to
  handle exposures and to initialize the GL object.
  
  Note: We can NOT constuct or execute any GL commands
  at this point because we have not yet been realized.
  Therefore, initialization of the GL odjects must be
  done in a seperate realization function.  In this
  case we will connect it to initgl().

  (the show signal happens before realization and should 
   not be used.)
***********************************************************/
Planet_Display::Planet_Display():Gtk::GLArea() 
  {
   planet=0;
   xAngle=yAngle=zAngle=zRot=0;

   //select events so that we can properly connect to them
     //GDK_POINTER_MOTION_HINT_MASK is used so that we 
     //don't get every point of motion
   set_events(GDK_EXPOSURE_MASK|
              GDK_BUTTON_MOTION_MASK|
              GDK_POINTER_MOTION_HINT_MASK|
              GDK_BUTTON_PRESS_MASK|
              GDK_BUTTON_RELEASE_MASK);

  }

void Planet_Display::realize_impl()
  {
   Gtk::GLArea::realize_impl();
   initgl(); 
  }

gint Planet_Display::motion_notify_event_impl(GdkEventMotion *event)
  {gdouble x,y;
   gfloat xChange,yChange;
   GdkModifierType state;

   // Calculate the relative motion of the cursor
   if (event->is_hint)
     {gint tx,ty;
      gdk_window_get_pointer(event->window, &tx, &ty, &state);
      x=gdouble(tx);
      y=gdouble(ty);
     } 
    else 
     {x = event->x;
      y = event->y;
      state=GdkModifierType(event->state);
     }
   if (!(state&GDK_BUTTON1_MASK)) return TRUE;
   xChange=-0.25*(GLfloat)(basex-x);
   yChange=-0.25*(GLfloat)(basey-y);
   basex=event->x;
   basey=event->y;

   
   // Transform curser motions into object rotations
   yAngle+=cos(xAngle/180.0*M_PI)*xChange;
   zAngle-=sin(xAngle/180.0*M_PI)*xChange;
   xAngle+=yChange;
 
   // Limit the values of rotation
   while (xAngle<0)   xAngle+=360;
   while (xAngle>360) xAngle-=360;
   while (yAngle<0)   yAngle+=360;
   while (yAngle>360) yAngle-=360;
   while (zAngle<0)   zAngle+=360;
   while (zAngle>360) zAngle-=360;

   // Signal the scrollbars which the new values
   updatex(xAngle);
   updatey(yAngle);
   updatez(zAngle);

   // redraw the object
   drawgl();
   return TRUE;
  }

gint Planet_Display::button_press_event_impl(GdkEventButton *event) 
  {basex=event->x;
   basey=event->y;
   return TRUE;
  }
gint Planet_Display::button_release_event_impl(GdkEventButton *event) 
  {return TRUE;}

gint Planet_Display::configure_event_impl(GdkEventConfigure *event)
  {
   if (make_current())
     {
      setupgl();
     }
   return TRUE;
  }

gint Planet_Display::expose_event_impl(GdkEventExpose *event)
  {
   // we are CPU intensive so we should wait for the last event
   if (event->count>0) return 0;
   drawgl();
   return TRUE;
  }

// Function has not been test (as far as I know)
Planet_Display::~Planet_Display()
  {if (glIsList(planet)==GL_TRUE)
     glDeleteLists(planet,1);
  }

/***********************************************************
***** Planet_Display::initgl
************************************************************
  This function creates the OpenGL list that will be used 
  to draw the planet.  We cannot do any GL rendering until
  the widget is realized including creating lists.
***********************************************************/
void Planet_Display::initgl()
  {
   int texture[40*MAX_GLOBE_SECTORS];
   int i;
   int pole_sectors,pole_belts,equator_belts;

   // In order to allow the window to be shrunk we will 
   // reset the minumum size to something smaller now 
   // that the inital allocation has been completed.
   //  (this works, but is this what they intended??)
   set_usize(100,100);

   // make_current must be called before performing GL operations
   if (make_current())
     {
      setupgl();

      planet = glGenLists(1);
      glNewList(planet, GL_COMPILE);
      pole_sectors=5;
      pole_belts=5;
      equator_belts=5;
      for (i=0;i<40*MAX_GLOBE_SECTORS;i++)
        texture[i]=rand();
      globeGL(4.0,pole_sectors,pole_belts,equator_belts,texture,20);
      glEndList();

     }
  }

/***********************************************************
***** Planet_Display::setupgl
************************************************************
  This function sets up the projection matrix and
  configures the OpenGL rendering engine. 

  This is called once to set up the initial matrix and
  then again if the window is resized.
***********************************************************/
void Planet_Display::setupgl()
  {
   static GLfloat pos[4] = {5.0, 5.0, 10.0, 0.0 };

   // Setup the rendering engine
//    glDepthFunc(GL_LESS);    // GL_LESS is the default
//    glClearDepth(1.0);       // 1.0 is the default
   glClearColor(0.0, 0.0, 0.0, 0.0); // frame buffer clears should be to black
   glLightfv( GL_LIGHT0, GL_POSITION, pos );
   glEnable( GL_CULL_FACE );
   glEnable( GL_LIGHTING );
   glEnable( GL_LIGHT0 );
   glEnable( GL_DEPTH_TEST );
   glEnable( GL_NORMALIZE );
   
   glViewport(0, 0, (GLint)(width()), (GLint)(height()));

   // Create the projection matrix
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   if (width()>height()) {
      GLfloat w = (GLfloat) width() / (GLfloat) height();
      glFrustum( -w, w, -1.0, 1.0, 5.0, 60.0 );
   }
   else {
      GLfloat h = (GLfloat) height() / (GLfloat) width();
      glFrustum( -1.0, 1.0, -h, h, 5.0, 60.0 );
   }

   // Go to the place to start the rendering
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glTranslatef( 0.0, 0.0, -40.0 );
   //glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  }

/***********************************************************
***** Planet_Display::drawgl
************************************************************
  This function just calls the OpenGL list to display the
  object and then commits the rendering scene.
***********************************************************/
void Planet_Display::drawgl() 
  {

   // start opengl rendering, you must put all opengl calls
   // inside make_current 
   if (make_current())
     {
   
      glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

      glPushMatrix();
        glRotatef( xAngle, 1.0, 0.0, 0.0 );
        glRotatef( yAngle, 0.0, 1.0, 0.0 );
        glRotatef( zAngle+zRot, 0.0, 0.0, 1.0 );

        glPushMatrix();
          glCallList(planet);
        glPopMatrix();

      glPopMatrix();
     }
   
   swap_buffers();
  }

// call backs to setup rotations scrollbars and idle function
void Planet_Display::setx(Gtk::Adjustment *adj)
  {xAngle=adj->get_value();
   drawgl();
  }
void Planet_Display::sety(Gtk::Adjustment *adj)
  {yAngle=adj->get_value();
   drawgl();
  }
void Planet_Display::setz(Gtk::Adjustment *adj)
  {zAngle=adj->get_value();
   drawgl();
  }
int Planet_Display::idle()
  {
   zRot+=1.0;
   if (zRot>360) zRot-=360;
   drawgl();
   return 1;  // we always want to be called so return 1
  }

/*********************************************************************
***** MainWindow
**********************************************************************
  This class will be our MainWindow containing an OpenGL window 
  and a toolbox to manipulate the OpenGL object. 
*********************************************************************/

class MainWindowClass : public Gtk::Window 
  {private:
     // Pointers to all of the widgets
     Gtk::Button *button;
     Gtk::Box *box;
     Gtk::Frame *frame;
     Gtk::Label *labelx,*labely,*labelz;
     Gtk::Table *toolbox;
     Gtk::Adjustment  *adjx,*adjy,*adjz;
     Gtk::Scrollbar *scrollx,*scrolly,*scrollz;
     Planet_Display *planet;
   public:
      MainWindowClass (void);
      ~MainWindowClass (void);
      gint delete_event_impl(GdkEventAny *) 
        {Gtk::Main::quit();
         return 0;
        }
  };

/***********************************************************
***** MainWindowClass::MainWindowClass
************************************************************
  Creates the main window and all of its widgets in a 
  bottom approach.  (I just find the style more readable
  than top down.)  View the file layout.ps for a description
  of the widget relations.
***********************************************************/
MainWindowClass::MainWindowClass(void) : Gtk::Window(GTK_WINDOW_TOPLEVEL)
  {

   set_title("Gtk-- GL Demo");
 
   // Create the OpenGL object (the whole point of this exercise) 
   planet = new Planet_Display();
   planet->set_usize(300,300);
   planet->show();

   // Gtk::GLArea can't have a border, so we will stick it in a frame
   frame = new Gtk::Frame();
   frame->set_shadow_type (GTK_SHADOW_IN);
   frame->set_border_width(10);  
   frame->add(*planet);   
   frame->show();

   // Make 3 scrollbars to control the planets orientation
   adjx=new Gtk::Adjustment(0,0,360+30,5,30,30);
   scrollx = new Gtk::VScrollbar(*adjx);
   scrollx->set_usize(15,100);
   scrollx->show();

   adjy=new Gtk::Adjustment(0,0,360+30,5,30,30);
   scrolly = new Gtk::VScrollbar(*adjy);
   scrolly->set_usize(15,100);
   scrolly->show();

   adjz=new Gtk::Adjustment(0,0,360+30,5,30,30);
   scrollz = new Gtk::VScrollbar(*adjz);
   scrollz->set_usize(15,100);
   scrollz->show();

   // Create a Quit button
   button = new Gtk::Button("Quit");
   button->set_usize(100,50);
   button->set_border_width(10);
   button->show();

   // Place the Scrollbars and Button in a Table
   // I was using attach_defaults, but it is better if just the scrollbars 
   // grow, to fill the space.
   toolbox = new Gtk::Table(3,3,FALSE);
   toolbox->attach(*scrollx,0,1,0,1,GTK_FILL,GTK_FILL|GTK_EXPAND|GTK_SHRINK,0,0);
   toolbox->attach(*scrolly,1,2,0,1,GTK_FILL,GTK_FILL|GTK_EXPAND|GTK_SHRINK,0,0);
   toolbox->attach(*scrollz,2,3,0,1,GTK_FILL,GTK_FILL|GTK_EXPAND|GTK_SHRINK,0,0);
     // Add some labels 
     labelx=new Gtk::Label("X");
     labelx->show();
     toolbox->attach(*labelx,0,1,1,2,0,0,0,0);
     labely=new Gtk::Label("Y");
     labely->show();
     toolbox->attach(*labely,1,2,1,2,0,0,0,0);
     labelz=new Gtk::Label("Z");
     labelz->show();
     toolbox->attach(*labelz,2,4,1,2,0,0,0,0);
   toolbox->attach(*button,0,3,2,3,GTK_SHRINK,GTK_SHRINK,0,0);
   toolbox->set_col_spacing(0,10);
   toolbox->set_col_spacing(1,10);
   toolbox->set_row_spacing(1,10);
   toolbox->set_border_width(10);
   toolbox->show();

   // Pack the toolbar and OpenGL frame together
   box = new Gtk::HBox(FALSE,0);
   box->pack_start(*frame,TRUE,TRUE,0);
   box->pack_start(*toolbox,FALSE,FALSE,0);
   box->show();

   // Add the box to the mainwindow
   add(*box);

   // Connect the Quit button to the application quit
   button->clicked.connect(Gtk::Main::quit.slot());

   // Connect scrollbars to the OpenGL object
   adjx->value_changed.connect(bind(slot(planet,&Planet_Display::setx),adjx));
   adjy->value_changed.connect(bind(slot(planet,&Planet_Display::sety),adjy));
   adjz->value_changed.connect(bind(slot(planet,&Planet_Display::setz),adjz));
 
   // Connect the OpenGL object back to the scrollbars 
   planet->updatex.connect(slot(adjx,&Gtk::Adjustment::set_value));
   planet->updatey.connect(slot(adjy,&Gtk::Adjustment::set_value));
   planet->updatez.connect(slot(adjz,&Gtk::Adjustment::set_value));

   // Setup the idle function to make the globe rotate
   Gtk::Main::idle.connect(slot(planet,&Planet_Display::idle));
  }

MainWindowClass::~MainWindowClass(void)
   { 
    delete button;
    delete box;
    delete frame;
    delete labelx;
    delete labely;
    delete labelz;
    delete toolbox;
    delete adjx;
    delete adjy;
    delete adjz;
    delete scrollx;
    delete scrolly;
    delete scrollz;
    delete planet;
   };

/***********************************************************
***** Main 
************************************************************
  very simple main body
***********************************************************/
gint main(gint argc, gchar **argv)
  {
   MainWindowClass *main_window1;
  
   Gtk::Main app(argc,argv);

   main_window1= new MainWindowClass;
   main_window1->show();
  
   Gtk::Main::run();

   return(0);
  }

