/*********************************************************************
  Copyright 1998 Karl Nelson <kenelson@ece.ucdavis.edu>
 
  This is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
 
  You should have received a copy of the GNU General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*********************************************************************/
#include <math.h>
#include <GL/gl.h>
#include "globeGL.hh"

/*********************************************************************
***** globeGL
**********************************************************************
  This draws a nice texture mapped globe made of triangles.
  
  For best results: the number of equator_belts should be between
    the number of pole_belts and twice the number of pole_belts.
    The number of pole_sectors should be between 4 and 7.

  The number of sectors arround the center is the number of
  pole_sectors times the pole_belts.  The total number of 
  faces on the object is approx to 
    pole_belts*pole_sectors*(equator_belts+pole_belts)
*********************************************************************/
#define MAX_INT 0x7FFFFFFF

#define setcolor(x) color[0]=((x)&0xFF000000),color[1]=(((x)&0x00FF0000)<<8),color[2]=(((x)&0x0000FF00)<<16)
int globeGL(float sphere_radius,int pole_sectors,
                int pole_belts,int equator_belts,int *texture,int rows)
  {
   float radius=0;
   static float x1[MAX_GLOBE_SECTORS+1],x2[MAX_GLOBE_SECTORS+1],
                y1[MAX_GLOBE_SECTORS+1],y2[MAX_GLOBE_SECTORS+1];
   static int   tex1[MAX_GLOBE_SECTORS],tex2[MAX_GLOBE_SECTORS];
   float z1,z2;
   float step,phase;
   int i,j,k;
   int sectors=pole_sectors;
   int pbelt,belt=1,belts=(pole_belts*2)+equator_belts;
   float rad_belt=M_PI/belts;
   int *tptr;

   // make sure they haven't requested too many sectors
   if (pole_sectors*pole_belts>MAX_GLOBE_SECTORS)
     return -1;
 
   static GLint color[4]={MAX_INT,MAX_INT,MAX_INT,MAX_INT};

   glShadeModel( GL_SMOOTH );

   // Top pole
   z2=sphere_radius*cos(rad_belt);
   radius=sphere_radius*sin(rad_belt);
   step=2*M_PI/sectors;
   tptr=&texture[rows];
   for (i=0;i<sectors+1;i++)
     {x2[i]=radius*cos(step*i);
      y2[i]=radius*sin(step*i);
      tex2[i]=tptr[i];
     }
   tex2[sectors]=tptr[0];
   glFrontFace(GL_CCW);      
   glBegin(GL_TRIANGLE_FAN);
   setcolor(texture[0*rows]);
   glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
   glNormal3f( 0.0, 0.0, GLfloat(sphere_radius));
   glVertex3f( 0.0, 0.0, GLfloat(sphere_radius));
   for (i=0;i<sectors+1;i++)
     {
      setcolor(tex2[i]);
      glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
      glNormal3f(GLfloat(x2[i]),GLfloat(y2[i]),GLfloat(z2));
      glVertex3f(GLfloat(x2[i]),GLfloat(y2[i]),GLfloat(z2));
     }
   glEnd();
   // expanding cap 
   glFrontFace(GL_CW);      
   for (belt++;belt<pole_belts+1;belt++)
     {
      // copy sectors
      z1=z2;
      for (i=0;i<sectors+1;i++)
        {x1[i]=x2[i];
         y1[i]=y2[i];
         tex1[i]=tex2[i];
        }
       
      //generate next belt
      sectors+=pole_sectors;
      z2=sphere_radius*cos(belt*rad_belt);
      radius=sphere_radius*sin(belt*rad_belt);
      step=2*M_PI/sectors;
      tptr=&texture[belt*rows];
      for (i=0;i<sectors+1;i++)
        {
         x2[i]=radius*cos(step*i);
         y2[i]=radius*sin(step*i);
         tex2[i]=tptr[i];
        }
      tex2[sectors]=tptr[0];
      k=0;
      for (j=0;j<pole_sectors;j++)
        {
         glBegin(GL_TRIANGLE_STRIP);
         for (i=j*belt;i<j*belt+belt;i++,k++)
           {
            setcolor(tex2[i]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x2[i],y2[i],z2);
            glVertex3f(x2[i],y2[i],z2);
      
            setcolor(tex1[k]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x1[k],y1[k],z1);
            glVertex3f(x1[k],y1[k],z1);
           }
         setcolor(tex2[i]);
         glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
         glNormal3f(x2[i],y2[i],z2);
         glVertex3f(x2[i],y2[i],z2);
         glEnd();
         k--;
        }
     } 
   glFrontFace(GL_CCW);      

   // equatoral bands 
   step=2*M_PI/sectors;
   phase=0;
   for (belt;belt<belts-pole_belts+1;belt++)
     {z1=z2;
      z2=sphere_radius*cos(belt*rad_belt);
      radius=sphere_radius*sin(belt*rad_belt);
      for (i=0;i<sectors+1;i++)
        {x1[i]=x2[i];
         y1[i]=y2[i];
         tex1[i]=tex2[i];
        }
      if (phase==0)
        {phase=step/2;
         tptr=&texture[belt*rows];
         for (i=0;i<sectors+1;i++)
           {
            x2[i]=radius*cos(step*i+phase);
            y2[i]=radius*sin(step*i+phase);
            tex2[i]=tptr[i];
           }
         tex2[sectors]=tptr[0];

         glBegin(GL_TRIANGLE_STRIP);
         for (i=0;i<sectors+1;i++)
           {
            setcolor(tex1[i]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x1[i],y1[i],z1);
            glVertex3f(x1[i],y1[i],z1);

            setcolor(tex2[i]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x2[i],y2[i],z2);
            glVertex3f(x2[i],y2[i],z2);
           }
         glEnd();
        }
       else
        {phase=0;
         tptr=&texture[belt*rows];
         for (i=0;i<sectors+1;i++)
           {
            x2[i]=radius*cos(step*i+phase);
            y2[i]=radius*sin(step*i+phase);
            tex2[i]=tptr[i];
           }
         tex2[sectors]=tptr[0];

         glBegin(GL_TRIANGLE_STRIP);
         setcolor(tex1[sectors]);
         glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
         glNormal3f(x1[sectors],y1[sectors],z1);
         glVertex3f(x1[sectors],y1[sectors],z1);
         for (i=0;i<sectors+1;i++)
           {
            setcolor(tex2[i]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x2[i],y2[i],z2);
            glVertex3f(x2[i],y2[i],z2);

            setcolor(tex1[i]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x1[i],y1[i],z1);
            glVertex3f(x1[i],y1[i],z1);
           }
         glEnd();
       }
    }

   // deceasing cap 
   glFrontFace(GL_CCW);      
   for (belt;belt<belts;belt++)
     {
      // copy sectors
      z1=z2;
      for (i=0;i<sectors+1;i++)
        {x1[i]=x2[i];
         y1[i]=y2[i];
         tex1[i]=tex2[i];
        }
       
      //generate next belt
      sectors-=pole_sectors;
      z2=sphere_radius*cos(belt*rad_belt);
      radius=sphere_radius*sin(belt*rad_belt);
      step=2*M_PI/sectors;
      tptr=&texture[belt*rows];
      for (i=0;i<sectors+1;i++)
        {
         x2[i]=radius*cos(step*i);
         y2[i]=radius*sin(step*i);
         tex2[i]=tptr[i];
        }
      tex2[sectors]=tptr[0];

      k=0;
      pbelt=belts-belt+1;
      for (j=0;j<pole_sectors;j++) //pole_sectors;j++)
        {
         glBegin(GL_TRIANGLE_STRIP);
         for (i=j*pbelt;i<j*pbelt+pbelt;i++,k++)
           {
            setcolor(tex1[i]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x1[i],y1[i],z1);
            glVertex3f(x1[i],y1[i],z1);
      
            setcolor(tex2[k]);
            glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
            glNormal3f(x2[k],y2[k],z2);
            glVertex3f(x2[k],y2[k],z2);
           }
         setcolor(tex1[i]);
         glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
         glNormal3f(x1[i],y1[i],z1);
         glVertex3f(x1[i],y1[i],z1);
         glEnd();
         k--;
        }
     }

   // Bottom Pole
   glFrontFace(GL_CW);      
   glBegin(GL_TRIANGLE_FAN);

   setcolor(texture[(belts-1)*rows]);
   glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
   glNormal3f( 0.0, 0.0, -sphere_radius);
   glVertex3f( 0.0, 0.0, -sphere_radius);

   for (i=0;i<sectors+1;i++)
     {
      setcolor(tex2[i]);
      glMaterialiv( GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color );
      glNormal3f(x2[i],y2[i],z2);
      glVertex3f(x2[i],y2[i],z2);
     }
   glEnd();
   glFrontFace(GL_CCW);      
  }



